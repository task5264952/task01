const user = require('../../model/userModel');


module.exports = {
    //=============================================User APIs=========================================================================================
    saveUser: async (req, res) => {
        const { name, age } = req.body;

        if (!name || !age) {
            return res.status(400).json({ error: 'Name and age are required' });
          }
        
        try {
            const userInfo = new user({ name, age });
            const saveUser = await userInfo.save();
            return res.send({ responseCode: 200, responseMessage: "Successfully Saved", responseResult: saveUser });

        }
        catch (error) {
            console.log(error)
            return res.send({ responseCode: 501, responseMessage: "Something went wrong", responseResult: error });
        }
    },

    getUser: async (req, res) => {
        try {
            const { name, sortBy, sortOrder, page, limit } = req.query;
        
            let query = {};
            if (name) {
              query.name = new RegExp(name, 'i'); // Case-insensitive search by name
            }
        
            let sortOptions = {};
            if (sortBy && ['age'].includes(sortBy)) {
              sortOptions[sortBy] = sortOrder === 'desc' ? -1 : 1;
            }
        
            const options = {
              sort: sortOptions,
              skip: (page - 1) * limit,
              limit: parseInt(limit),
            };
        
            const users = await user.find(query, null, options);
            res.json(users);
          } catch (error) {
            console.error(error);
            res.status(500).json({ error: 'Internal server error' });
          }
    },

}
