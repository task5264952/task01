const userRouter = require("express").Router();
const usercontroller = require('./userController');

userRouter.post('/create-user', usercontroller.saveUser);
userRouter.get('/get-user', usercontroller.getUser)


module.exports = userRouter;
