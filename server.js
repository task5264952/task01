const { urlencoded } = require('body-parser');
const express = require('express');
const db = require('./db/dbconnection')
const userRouter = require('./controller/User/userRouter');
const app = express();
const port = 7000;

app.use(express.urlencoded({ limit: '50mb', extended: false }));
app.use(express.json({ limit: '50mb' }));
app.use('/user', userRouter);

app.use('/api/v1/', userRouter);

app.listen(port, (error, result) => {
    if (error) {
        console.log(`server is not connected`);
    }
    else {
        console.log(`server is connected on port:`,port);
    }
})
